
var tasks=[];
function addFun(){
    const name=document.getElementById("inputname").value;
    tasks[tasks.length]= {
        id:tasks.length+1,
        name
    }
    const len = tasks.length;
    let content ="";
    for(let i = 0; i < len; i++){
        content +=`
        <div class="task" id="task_${tasks[i].id}">
            <input type="checkbox" id="checkbox_${tasks[i].id}">
            <button onclick='enableEdit(${tasks[i].id})'>${tasks[i].name}</button>
            <button class="delete" onclick='deleteFun(${tasks[i].id})'>
                x
            </button>
        </div>
    `;
    }
    document.getElementById("task").innerHTML=content;
}
function enableEdit(id){
    const task = tasks.find((e)=>e.id===id);
    const taskEle = document.getElementById("task_"+id);
    const edit = `<input type="text" id="task_edit_${id}" value="${task.name}" onblur="disableEdit(this.value,${id})">`;
    taskEle.innerHTML = edit;
}

function disableEdit(value,id) {
    console.log(value+"   "+id);
}